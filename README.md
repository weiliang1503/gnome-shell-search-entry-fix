# GNOME Overview Search Entry Backspace Button Crash Fix

Install this repo as a gnome-shell extension, it will hide the backspace button, so you'll never click it to cause a crash.
