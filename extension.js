const m = imports.ui.main;

let se = null;
let id = null;

function init() {
    se = m.overview.searchEntry;
}

function enable() {
    id = se.connect("notify::text", () => {
	if (se.get_child_at_index(3) != null) {
	    se.get_child_at_index(3).destroy() } }
		   );
}

function disable() {
    se.disconnect(id);
}
